﻿using System.Diagnostics;

namespace DumpHelper.Monitor
{
    public class ComputerParameters
    {
        public string Name { get; private set; }
        public double CpuUsage { get; private set; }
        public double RamUsage { get; private set; }

        private PerformanceCounter CpuCounter;
        private PerformanceCounter RamCounter;

        public ComputerParameters(string name)
        {
            Name = name;
            //todo сделать запуск чтения параметров под конкретным пользователем
            //CpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total", Name);
            //RamCounter = new PerformanceCounter("Memory", "Available MBytes", "", Name);
            Update();
        }

        public void Update()
        {
            //CpuUsage = CpuCounter.NextValue();
            //RamUsage = RamCounter.NextValue() / 1024.0;
        }
    }
}
