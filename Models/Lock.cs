﻿using System.ComponentModel.DataAnnotations;

namespace DumpHelper.Models
{
    public class Lock
    {
        public string OWNER { get; set; }
        public string OBJECT_NAME { get; set; }
        public string OBJECT_TYPE { get; set; }
        [Key]
        public string SID { get; set; }
        public string STATUS { get; set; }
        public string OSUSER { get; set; }
        public string MACHINE { get; set; }
    }
}
