﻿using System.ComponentModel.DataAnnotations;

namespace DumpHelper.Models
{
    public class User
    {
        [Key]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [MaxLength(200)]
        [DataType(DataType.Text)]
        public string FirstName{ get; set; }

        [MaxLength(200)]
        [DataType(DataType.Text)]
        public string SecondName { get; set; }
    }
}
