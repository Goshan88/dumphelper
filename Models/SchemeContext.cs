﻿
using DumpHelper.Models;
using Microsoft.EntityFrameworkCore;

namespace DumpHelper.Models
{
    public class SchemeContext : DbContext
    {
        public DbSet<Scheme> Schemes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Session> SESSIONS { get; set; }
        public DbSet<Lock> LOCKS { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var settings = Settings.GetInstance();
            var connectionString = settings.GetConnectionString("DefaultConnection");
            try
            {
                optionsBuilder.UseOracle(connectionString);
            }catch(Exception ex)
            {
                Console.WriteLine("Error in connection to database: " + ex.Message);
            }
        }

        public SchemeContext()
        {
            Database.EnsureCreated();
        }
    }
}
