﻿using System.ComponentModel.DataAnnotations;

namespace DumpHelper.Models
{
    public class Scheme
    {
        [Key]
        [MaxLength(100)]
        public string NameId { get; set; }

        public DateTime Start{ get; set; }

        public DateTime Finish { get; set; }
                    
        [MaxLength(200)]
        public string OfficialName { get; set; }

        [MaxLength(100)]
        public string Code { get; set; }
        
        [MaxLength(100)]
        public string Server { get; set; }

        [MaxLength(20)]
        public string ClientVersion { get; set; }

    }
}
