﻿using System.ComponentModel.DataAnnotations;

namespace DumpHelper.Models
{
    public class Session
    {
        [Key]
        public uint SID { get; set; }
        public string OSUSER { get; set; }
        public string SCHEMANAME { get; set; }
        public string STATUS { get; set; }
        public string PROGRAM { get; set; }
    }
}
