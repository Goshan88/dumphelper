﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DumpHelper.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Schemes",
                columns: table => new
                {
                    NameId = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: false),
                    Start = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false),
                    Finish = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false),
                    OfficialName = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: false),
                    Code = table.Column<string>(type: "NVARCHAR2(40)", maxLength: 40, nullable: false),
                    Server = table.Column<string>(type: "NVARCHAR2(40)", maxLength: 40, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schemes", x => x.NameId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Schemes");
        }
    }
}
