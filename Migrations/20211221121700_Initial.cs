﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DumpHelper_v1.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Schemes",
                columns: table => new
                {
                    NameId = table.Column<string>(type: "NVARCHAR2(450)", nullable: false),
                    OfficialName = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: false),
                    Code = table.Column<string>(type: "NVARCHAR2(20)", maxLength: 20, nullable: false),
                    Server = table.Column<string>(type: "NVARCHAR2(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schemes", x => x.NameId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Schemes");
        }
    }
}
