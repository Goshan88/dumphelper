﻿using DumpHelper.Models;
using DumpHelper.Monitor;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace DumpHelper.Controllers
{
    public class MonitorController : Controller
    {
        private ComputerParameters DBComputer;
        private SchemeContext database = new SchemeContext();
        public MonitorController()
        {
            Console.WriteLine("MonitorController constructor work");
            var settings = Settings.GetInstance();
            var nameDbComputer = settings.GetString("ServerWithDataBase");
            DBComputer = new ComputerParameters(nameDbComputer);
            //Thread.Sleep(1000); потому что пока функционал чтения параметров компьютера не работает
            DBComputer.Update();
        }
        
        public IActionResult Index()
        {
            Console.WriteLine("MonitorController index method work");
            ViewBag.ComputerName = DBComputer.Name;
            ViewBag.CpuCounter = Math.Round(DBComputer.CpuUsage, 2);
            ViewBag.RamCounter = Math.Round((DBComputer.RamUsage), 2);

            var sessions = database.SESSIONS.OrderBy(x => x.OSUSER);
            ViewBag.Sessions = sessions;
            var locks = database.LOCKS.OrderBy(x => x.OWNER);
            ViewBag.Locks = locks;
            
            return View();
        }
    }
}
