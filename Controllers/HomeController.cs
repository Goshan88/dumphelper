﻿using DumpHelper.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore;

namespace DumpHelper.Controllers
{
    public class HomeController : Controller
    {
        private SchemeContext database = new SchemeContext();
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            Console.WriteLine("Index method work");
            var schemes = database.Schemes.OrderBy(x=>x.OfficialName);
            ViewBag.Schemes = schemes;
            return View();
        }

        public IActionResult Privacy()
        {
            Console.WriteLine("Privacy method");
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult UpdateScheme()
        {
            var settings = Settings.GetInstance();
            var updateSchemsInfoCommand = settings.GetString("UpdateSchemsInfoCommand");
            database.Database.ExecuteSqlRaw(updateSchemsInfoCommand);
            return RedirectToAction("Index");
        }
    }
}