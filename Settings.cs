﻿namespace DumpHelper
{
    public sealed class Settings
    {
        public static Settings GetInstance()
        {
            if (instance == null)
            {
                instance = new Settings();
            }
            return instance;
        }

        private Settings()
        {
            builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            config = builder.Build();
        }

        public string GetConnectionString(string name) => config.GetConnectionString(name);
        
        public string GetString(string name) => config.GetValue(name, "");

        private static Settings instance;
        private ConfigurationBuilder builder;
        private IConfiguration config; 
    }
}
